#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#define MAX_COMMAND_LENGTH 100
#define MAX_NUMBER_OF_PARAMS 20
#define READ 0
#define WRITE 1


struct processes{
	char* cmdname[100];
    int pid[100];
	int total;
};

struct processes add_procc(struct processes proc,char* name,int pid){
	proc.pid[proc.total] = pid;
	proc.cmdname[proc.total] = name;
	proc.total++;
	
	return proc;
}

void jobs(struct processes proc){
	printf("\nProcesses : ");
	int i =0;
	for(i; i < proc.total ; i++){
		printf("Process name : %s, pid : %d\n",proc.cmdname[i],proc.pid[i]);
	}
	return;
}

void parseCmd(char* cmd, char** params);
int executeCmd(char** params);
int executeDaem(char** params);
int fork_handler (int num_of_commands, char** params);
int spawn_proc (int in, int out, char** params, int comm_offset);
int PipNum(char *cmd);


int main()
{
    char cmdBuff[MAX_COMMAND_LENGTH + 1];
    char* params[MAX_NUMBER_OF_PARAMS + 1];
    
	int isDaemon = 0; 
    int isPipe = 0;	
    int cmdCount = 0;
	
	char *find_char;
	int num_of_comm = 0;
    
	char *name;
    name = getlogin();
	if (!name) perror("getlogin() error");
	
	struct processes proc;
	proc.total = 0;
	proc = add_procc(proc ,"main", (int) getpid());
	
	char *curr_path;
	char *buff;
	
	/* 
	 * _PC_PATH_MAX is a macro that returns maximum length of 
	 * a filename in the current path 
	*/
	int size = pathconf(".", _PC_PATH_MAX);
	if ((buff = (char *)malloc((size_t)size)) != NULL)
        curr_path = getcwd(buff, (int)size);
	
	
    while(1) {
        /* Print command prompt */
        printf("\n%s@cs345sh/%s ", name, curr_path);

        /* Read command from standard input, exit on Ctrl+C */
        if(fgets(cmdBuff, sizeof(cmdBuff), stdin) == NULL) break;
		
		printf("\n");

        /* Remove trailing newline character, if any */
        if(cmdBuff[strlen(cmdBuff)-1] == '\n') 
            cmdBuff[strlen(cmdBuff)-1] = '\0';
        
		if(cmdBuff[strlen(cmdBuff)-1] == '&'){
			isDaemon = 1;
			cmdBuff[strlen(cmdBuff)-1] = '\0';
		}
		
		find_char = strchr(cmdBuff, '|');
		if(find_char != NULL){
			isPipe = 1;
			num_of_comm = comm_num(cmdBuff);
			find_char = NULL;			
		}
			
            
        /* Split cmdBuff into an array of strings or vector */
        parseCmd(cmdBuff, params);

        if(strcmp(params[0], "exit") == 0) break;
				
		if(strcmp(params[0], "cd") == 0){
			chdir(params[1]);
			curr_path = getcwd(buff, (int)size);
		}
		else if(strcmp(params[0], "jobs") == 0){
			jobs(proc);
		}
		/* is a command that needs exec to be called */
		else{
		    if(isDaemon){
			    isDaemon = 0;			
			    executeDaem(params);
		    }
		    else if(isPipe){
			    isPipe = 0;
			    fork_handler(num_of_comm, params);
		    }
		    else{
		        executeCmd(params);
		    }
	        memset(cmdBuff,'\0',strlen(cmdBuff));
        }
	}    

	free(buff);
    return 0;
}

/* Split cmd into array of parameters */
void parseCmd(char* cmd, char** params){       
    int i;
    for( i = 0; i < MAX_NUMBER_OF_PARAMS; i++) {
        params[i] = strsep(&cmd, " ");
        if(params[i] == NULL) break;
    }
}

int executeCmd(char** params){
    pid_t pid = fork();
    
    /* Error */
    if (pid == -1) {
        char* error = strerror(errno);
        printf("fork: %s\n", error);
        return 1;
    }

    // Child process
    else if (pid == 0) {
        // Execute command
        execvp(params[0], params);  

        // If exec executes correctly following code will not
		// be executed
        char* error = strerror(errno);
        printf("shell: %s: %s\n", params[0], error);
        return 0;
    }

    // Parent process
    else {
        
		// Wait for child process to finish
		int childStatus;
        waitpid(pid, &childStatus, 0);  
        return 0;
    }
}

int executeDaem(char** params){
    // Fork process
    pid_t pid = fork();
    
    // Error
    if (pid == -1) {
        char* error = strerror(errno);
        printf("fork: %s\n", error);
        return 1;
    }

    // Child process
    else if (pid == 0) {
        // Execute command
        execvp(params[0], params);  

        // If exec executes correctly following code will not
		// be executed
        char* error = strerror(errno);
        printf("shell: %s: %s\n", params[0], error);
        return 1;
    }

    // Parent process
    else {
        return 0;
    }
}

int comm_num(char *cmd){
	int i,count=0;
	for (i = 0; cmd[i] != '\0'; i++) {
      if (cmd[i] == '|')
         count++;
    }
	return count+1;
}

int fork_handler (int num_of_commands, char** params){
    int i;
    pid_t pid ,chpid;
	int childStatus;
    int in, fd [2];
    /* The first process should get its input from the original file descriptor 0  */
    in = 0;
	
	
	int commands[10];
	commands[0] = 0;
    int comm_offset = 0;
	int j = 1;
	/* An int array will store the starting place of each command within our parameter vector */
	for(i = 0; params[i] != NULL ; i++){
		if(!strcmp( params[i], "|")){
		    commands[j] = i+1;
			j++;
			/* replacing every | with a NULL */
			params[i] = NULL;
		}
	}
	
	pid = fork();
	if (pid == -1) {
        char* error = strerror(errno);
        printf("fork: %s\n", error);
        return 1;
    }
    if(pid == 0){
		pipe(fd);
    	for (i = 0; i < num_of_commands-1 ; ++i){
			    /* Keeping track of the commands */
			    comm_offset = commands[i];
			    /* 
			     * fd[WRITE] is the write part of the pipe 
			     * and it will be used by each child in place of stdout  
			     */
				 //printf("spawn proc called : %s\n", params[comm_offset]);
				 
                 spawn_proc(in, fd[WRITE], params, comm_offset);
        
                 /* Current process has no need for write end of pipe  */
                 close(fd[WRITE]);
        
                 /* Keep the read end of the pipe, the next child will read from here  */
                 in = fd[READ];
        }
		/* Last command */
		comm_offset = commands[i];
		
		/* Last command will have input from pipe */
	    dup2(in, READ);
		close(fd[WRITE]);
		/* But we will not close the standard output */
		/* Because we want the output to be printed on the screen */
		
	    //printf("command executed : %s\n", params[comm_offset]);
        
		/* Execute the last stage with the current process */		
        execvp(params[comm_offset], params + comm_offset);
    }else{
	    wait(pid, &childStatus, 0);		
		return 0;
	}
	return 1;
}

int spawn_proc (int in, int out, char** params, int comm_offset){
    pid_t pid;
    pid = fork();
	if (pid == -1) {
        char* error = strerror(errno);
        printf("fork: %s\n", error);
        return 1;
    }
    if (pid == 0){
        /* First child doesnt need to read from previous child */
        if (in != READ){
            dup2(in, READ);
            close(in);
        }
        
		/* Output will be redirected to write end of pipe */
        dup2(out, WRITE);

		close(out);

		// printf("command executed (spawn): %s\n", params[comm_offset]);
        execvp(params[comm_offset], params + comm_offset);
    }
    
    int childStatus;
    waitpid(pid, &childStatus, 0);
    return 0;
}